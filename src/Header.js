import React from 'react'

const Header = () => {
  return (
    <header className='bg-info text-white py-1 px-1 text-left rounded mb-4'>
      <h3>TODOs</h3>
    </header>
  )
}

export default Header;